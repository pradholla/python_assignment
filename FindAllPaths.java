package src;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Scanner;

class GraphFindAllPaths<T> implements Iterable<T> {

    public final Map<T, Map<T, Double>> graph = new HashMap<T, Map<T, Double>>();
    public boolean addNode(T node) {
        if (node == null) {
            throw new NullPointerException("The input node cannot be null.");
        }
        if (graph.containsKey(node)) return false;

        graph.put(node, new HashMap<T, Double>());
        return true;
    }
    public void addEdge (T source, T destination, double length) {
        if (source == null || destination == null) {
            throw new NullPointerException("Source and Destination, both should be non-null.");
        }
        if (!graph.containsKey(source) || !graph.containsKey(destination)) {
            throw new NoSuchElementException("Source and Destination, both should be part of graph");
        }
        /* A node would always be added so no point returning true or false */
        graph.get(source).put(destination, length);
    }
    public void removeEdge (T source, T destination) {
        if (source == null || destination == null) {
            throw new NullPointerException("Source and Destination, both should be non-null.");
        }
        if (!graph.containsKey(source) || !graph.containsKey(destination)) {
            throw new NoSuchElementException("Source and Destination, both should be part of graph");
        }
        graph.get(source).remove(destination);
    }
    public Map<T, Double> edgesFrom(T node) {
        if (node == null) {
            throw new NullPointerException("The node should not be null.");
        }
        Map<T, Double> edges = graph.get(node);
        if (edges == null) {
            throw new NoSuchElementException("Source node does not exist.");
        }
        return Collections.unmodifiableMap(edges);
    }
    @Override public Iterator<T> iterator() {
        return graph.keySet().iterator();
    }
}
public class FindAllPaths<T> {

	private final  GraphFindAllPaths<T> graph;
    public static  int Source;
	public static int Destination;
    public FindAllPaths(GraphFindAllPaths<T> graph) {
        if (graph == null) {
            throw new NullPointerException("The input graph cannot be null.");
        }
        this.graph = graph;
    }


    private void validate (T source, T destination) {

        if (source == null) {
            throw new NullPointerException("The source: " + source + " cannot be  null.");
        }
        if (destination == null) {
            throw new NullPointerException("The destination: " + destination + " cannot be  null.");
        }
        if (source.equals(destination)) {
            throw new IllegalArgumentException("The source and destination: " + source + " cannot be the same.");
        }
    }
    public List<List<T>> getAllPaths(T source, T destination) {
    	if (source == null || destination == null) {
            throw new NullPointerException("Source and Destination, both should be non-null.");
        }
        validate(source, destination);
        Map<List<T>,Double> pathWithCost = new HashMap<List<T>,Double>();
       
        List<List<T>> paths = new ArrayList<List<T>>();
        List<Double> totalCost = new ArrayList<Double>();
        Double cost = new Double(0);
        recursive(source, destination, paths, new LinkedHashSet<T>(),totalCost,cost, new HashMap<T, Double>());
        for(int i=0;i<paths.size();i++){
            pathWithCost.put(paths.get(i), totalCost.get(i));
        }
        return paths;
    }
    private void recursive (T current, T destination, List<List<T>> paths, LinkedHashSet<T> path, List<Double> totalCost,Double cost, Map<T, Double> allEdges) 
    {
        path.add(current);
        if(allEdges.get(current)!=null){
            cost= cost+allEdges.get(current); 
        }
        if (current == destination)
        {
            cost= cost+allEdges.get(current);
            paths.add(new ArrayList<T>(path));
            cost= cost-allEdges.get(current);
            totalCost.add(cost);
            path.remove(current);
            return;
        }

        allEdges = graph.edgesFrom(current);
        final Set<T> edges  = graph.edgesFrom(current).keySet();
        for (T t : edges) {
            if (!path.contains(t)) {
                recursive (t, destination, paths, path,totalCost,cost , allEdges);
            }
        }
        path.remove(current);
    }    
    public  Map<List<T>,Double> getAllPathsWithCost(T source, T destination) {
        validate(source, destination);
        Map<List<T>,Double> pathWithCost = new HashMap<List<T>,Double>();
        List<List<T>> paths = new ArrayList<List<T>>();
        List<Double> totalCost = new ArrayList<Double>();
        Double cost = new Double(0);
        recursiveWithCost(source, destination, paths, new LinkedHashSet<T>(),totalCost,cost, new HashMap<T, Double>());
        for(int i=0;i<paths.size();i++){
            pathWithCost.put(paths.get(i), totalCost.get(i));
        }
        return pathWithCost;
    }
    private void recursiveWithCost (T current, T destination, List<List<T>> paths, LinkedHashSet<T> path, List<Double> totalCost,Double cost, Map<T, Double> allEdges) {
        path.add(current);
        if(allEdges.get(current)!=null){
            cost= cost+allEdges.get(current);
        }
        if (current == destination) {
            cost= cost+allEdges.get(current);
            paths.add(new ArrayList<T>(path));

            cost= cost-allEdges.get(current);
            totalCost.add(cost);
            path.remove(current);
            return;
        }
        allEdges = graph.edgesFrom(current);
        final Set<T> edges  = graph.edgesFrom(current).keySet();
        for (T t : edges) {
            if (!path.contains(t)) {
                recursiveWithCost (t, destination, paths, path,totalCost,cost , allEdges);
            }
        }
        path.remove(current);
    }    
    public static void main(String[] args) {
    	System.out.println("Enter the Source(Number) :\n0:Mysore 1:Mandya 2:Chennapatna 3:Nanjangud 4:Bandipur 5:Nagarhole 6:Somnmathpur 7:Bylakuppe");
        Scanner sc=new Scanner(System.in);
        Source=sc.nextInt();
        System.out.println("Enter the Destination(Number) :\n0:Mysore 1:Mandya 2:Chennapatna 3:Nanjangud 4:Bandipur 5:Nagarhole 6:Somnmathpur 7:Bylakuppe");
        Destination=sc.nextInt();
        GraphFindAllPaths<String> graphFindAllPaths = new GraphFindAllPaths<String>();
        graphFindAllPaths.addNode("Mysore");
        graphFindAllPaths.addNode("Mandya");
        graphFindAllPaths.addNode("Chennapatna");
        graphFindAllPaths.addNode("Nanjangud");
        graphFindAllPaths.addNode("Bandipur");
        graphFindAllPaths.addNode("Nagarhole");
        graphFindAllPaths.addNode("Somnathpur");
        graphFindAllPaths.addNode("Bylakuppe");

        graphFindAllPaths.addEdge("Mysore", "Mandya", 66);
        graphFindAllPaths.addEdge("Mysore", "Chennapatna", 28);
        graphFindAllPaths.addEdge("Mysore", "Nanjangud", 60);
        graphFindAllPaths.addEdge("Mysore", "Bandipur", 34);
        graphFindAllPaths.addEdge("Mysore", "Nagarhole", 34);
        graphFindAllPaths.addEdge("Mysore", "Somnathpur", 3);
        graphFindAllPaths.addEdge("Mysore", "Bylakuppe", 108);
        
        graphFindAllPaths.addEdge("Mandya", "Mysore", 66);
        graphFindAllPaths.addEdge("Mandya", "Chennapatna", 22);
        graphFindAllPaths.addEdge("Mandya", "Nanjangud", 12);
        graphFindAllPaths.addEdge("Mandya", "Bandipur", 91);
        graphFindAllPaths.addEdge("Mandya", "Nagarhole", 121);
        graphFindAllPaths.addEdge("Mandya", "Somnathpur", 111);
        graphFindAllPaths.addEdge("Mandya", "Bylakuppe", 71);
        
        graphFindAllPaths.addEdge("Chennapatna", "Mysore", 28);
        graphFindAllPaths.addEdge("Chennapatna", "Mandya", 22);
        graphFindAllPaths.addEdge("Chennapatna", "Nanjangud", 39);
        graphFindAllPaths.addEdge("Chennapatna", "Bandipur", 113);
        graphFindAllPaths.addEdge("Chennapatna", "Nagarhole", 130);
        graphFindAllPaths.addEdge("Chennapatna", "Somnathpur", 35);
        graphFindAllPaths.addEdge("Chennapatna", "Bylakuppe", 40);
        
        graphFindAllPaths.addEdge("Nanjangud", "Mysore", 60);
        graphFindAllPaths.addEdge("Nanjangud", "Mandya", 12);
        graphFindAllPaths.addEdge("Nanjangud", "Chennapatna", 39);
        graphFindAllPaths.addEdge("Nanjangud", "Bandipur", 63);
        graphFindAllPaths.addEdge("Nanjangud", "Nagarhole", 21);
        graphFindAllPaths.addEdge("Nanjangud", "Somnathpur", 57);
        graphFindAllPaths.addEdge("Nanjangud", "Bylakuppe", 83);
        
        graphFindAllPaths.addEdge("Bandipur", "Mysore", 34);
        graphFindAllPaths.addEdge("Bandipur", "Mandya", 91);
        graphFindAllPaths.addEdge("Bandipur", "Chennapatna", 113);
        graphFindAllPaths.addEdge("Bandipur", "Nanjangud", 63);
        graphFindAllPaths.addEdge("Bandipur", "Nagarhole", 9);
        graphFindAllPaths.addEdge("Bandipur", "Somnathpur", 50);
        graphFindAllPaths.addEdge("Bandipur", "Bylakuppe", 60);
        
        graphFindAllPaths.addEdge("Nagarhole", "Mysore", 34);
        graphFindAllPaths.addEdge("Nagarhole", "Mandya", 121);
        graphFindAllPaths.addEdge("Nagarhole", "Chennapatna", 130);
        graphFindAllPaths.addEdge("Nagarhole", "Nanjangud", 21);
        graphFindAllPaths.addEdge("Nagarhole", "Bandipur", 9);
        graphFindAllPaths.addEdge("Nagarhole", "Somnathpur", 27);
        graphFindAllPaths.addEdge("Nagarhole", "Bylakuppe", 81);
        
        graphFindAllPaths.addEdge("Somnathpur", "Mysore", 3);
        graphFindAllPaths.addEdge("Somnathpur", "Mandya", 111);
        graphFindAllPaths.addEdge("Somnathpur", "Chennapatna", 35);
        graphFindAllPaths.addEdge("Somnathpur", "Nanjangud", 57);
        graphFindAllPaths.addEdge("Somnathpur", "Bandipur", 50);
        graphFindAllPaths.addEdge("Somnathpur", "Nagarhole", 27);
        graphFindAllPaths.addEdge("Somnathpur", "Bylakuppe", 90);
        
        graphFindAllPaths.addEdge("Bylakuppe", "Mysore", 108);
        graphFindAllPaths.addEdge("Bylakuppe", "Mandya", 71);
        graphFindAllPaths.addEdge("Bylakuppe", "Chennapatna", 40);
        graphFindAllPaths.addEdge("Bylakuppe", "Nanjangud", 83);
        graphFindAllPaths.addEdge("Bylakuppe", "Bandipur", 60);
        graphFindAllPaths.addEdge("Bylakuppe", "Nagarhole", 81);
        graphFindAllPaths.addEdge("Bylakuppe", "Somnathpur", 90);
        String s=null,d=null;
        if(Source==0)
        	s="Mysore";
        else if(Source==1)
        	s="Mandya";
        else if(Source==2)
        	s="Chennapatna";
        else if(Source==3)
        	s="Nanjangud";
        else if(Source==4)
        	s="Bandipur";
        else if(Source==5)
        	s="Nagarhole";
        else if(Source==6)
        	s="Somnathpur";
        else if(Source==7)
        	s="Bylakuppe";
        else {
        	System.out.println("Invalid Source");
            return;
        }
        if(Destination==0)
        	d="Mysore";
        else if(Destination==1)
        	d="Mandya";
        else if(Destination==2)
        	d="Chennapatna";
        else if(Destination==3)
        	d="Nanjangud";
        else if(Destination==4)
        	d="Bandipur";
        else if(Destination==5)
        	d="Nagarhole";
        else if(Destination==6)
        	d="Somnathpur";
        else if(Destination==7)
        	d="Bylakuppe";
        else {
        	System.out.println("Invalid Source");
            return;
        }
        FindAllPaths<String> findAllPaths = new FindAllPaths<String>(graphFindAllPaths);
        
        System.out.println("All possible Paths : ");
                for (List<String> path :findAllPaths.getAllPaths(s,d))
        {
        	if(path.size()==8)
             System.out.println(path);
        }
        System.out.println("\nAll possible paths with total distance : ");
        Map<List<String>,Double> pathwithcost =  new HashMap<List<String>,Double>();
        Map<List<String>,Double> pathWithCost = findAllPaths.getAllPathsWithCost(s,d);
        for(Entry<List<String>,Double> ss : pathWithCost.entrySet())
        {
        	 List<String> path = ss.getKey();
        	 if(path.size()==8)
        	 {
        		pathwithcost.put(ss.getKey(),ss.getValue());
                System.out.println(ss);
        	 }
        }
        System.out.println("Minimum Cost Path: ");
        Entry<List<String>, Double> min =null;
        for (Entry<List<String>,Double> entry : pathwithcost.entrySet()) 
        {
            if (min == null || min.getValue() > entry.getValue())
            {
                min = entry;
            }        
        }
        System.out.println(min);
        sc.close();
    }
   }